### EXPERIMENT ON EXPRESSION EVALUATION
### Aim

            1.To learn about different types of operators.
            2.To learn about the precedence of the operators.
            3.To learn how to evaluate an expression.

### Theory

 <h3>Principle:</h3>
                     Evaluating an expression involves repeatedly solving the sub-expressions in the lowest level paranthesis and substituting its value to solve the bigger expression. If an expression or sub-expression does not have a paranthesis, then we can directly compute its value based on the precedence and assosciativity of the operators present in the expression. That is, the sub expression involving operator with higher precedence is evaluated before other sub-expressions
        <br>Operators can be broadly categorized as:
        
            1.Arithmetic Operators: These operators are used for arithmetic operations like addition,subtraction and multiplication. Ex: DIVISION(/),MODULUS OR REMAINDER (%)
            2.Relational Operators: These operators are typically used for comparison of two operands.Ex: GREATER THAN(>),GREATER THAN OR EQUAL TO(>=) etc.
            3.Logical Operators: These operators are used for conducting logical operations like AND,OR. Ex: LOGICAL AND( && ),LOGICAL OR (||) etc.
            4.Bitwise Operators: These operators are used to perform operations on bits. Ex: BITWISE AND( & ), BITWISE OR ( | ) etc.
                     <br><br>

#### Procedure

<h3>Procedure for the experiment is as follows</h3>
        1.Select the type of operators and the datatype you want to work upon from the top most bar.<br>
        2.You can edit the values of variables by pressing the edit button.<br>
        3.Select an expression prototype from the menu.<br>
        4.You can also edit this expression.<br>
        5.Press Next to see the step by step evaluation of the selected expression in the central panel and the corresponding reasoning in the right panel.<br>
        6.Press stop if you want to abort the experiment and start over.<br>
